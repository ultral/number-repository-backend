package pl.t_mobile;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import pl.t_mobile.model.RepositoryNumber;

import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

    @Test
    public void contextLoads() {}

    @Autowired
    @SuppressWarnings("unused")
    private TestRestTemplate restTemplate;

    @Test
    public void homeResponse() {
        final String body = restTemplate.getForObject("/", String.class);
        assertThat(body).isEqualTo("Spring is here!");
    }

    @Test
    public void putResponse() {
        final String msisdnParamName = "msisdn";
        final Map<String, String> params = Collections.singletonMap(msisdnParamName, "666555444");
        final String body = restTemplate.getForObject("/get?msisdn=666555444", String.class);
        assertThat(body).contains(params.get(msisdnParamName));
    }
}
