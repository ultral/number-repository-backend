package pl.t_mobile.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RepositoryNumber {
    private final String msisdn;

    public RepositoryNumber(final String msisdn) {
        this.msisdn = msisdn;
    }

    @JsonProperty(required = true)
    public String getMsisdn() {
        return msisdn;
    }
}
