package pl.t_mobile;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.t_mobile.model.RepositoryNumber;

@SuppressWarnings("unused")
@RestController
public class NumberController {

    @PutMapping(path = "/put", produces = "application/json")
    RepositoryNumber put(@RequestParam(value="msisdn") String msisdn) {
        return new RepositoryNumber(msisdn);
    }

    @GetMapping(path = "/get", produces = "application/json")
    RepositoryNumber get(@RequestParam(value="msisdn") String msisdn) {
        return new RepositoryNumber(msisdn);
    }
}
